<?php

require('animal.php');
require('ape.php');
require('frog.php');


$sheep = new Animal("shaun");
echo "<h3>Kambing</h3>";

echo "Name: ". $sheep->name . "<br>"; // "shaun"
echo "Legs: " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded: " . $sheep->cold_blooded . "<br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
echo "<h3>Monyet</h3>";
// index.php
$sungokong = new Ape("kera sakti");
echo "Name: " . $sungokong->name . "<br>";
echo "Legs: " . $sungokong->legs . "<br>";
echo "Cold Blooded: " . $sungokong->cold_blooded . "<br>";
echo "Yell: " . $sungokong->yell() . "<br><br>"; // "Auooo"
echo "<h3>Buduk</h3>";
$kodok = new Frog("buduk");
echo "Name: " . $kodok->name . "<br>";
echo "Legs: " . $kodok->legs . "<br>";
echo "Cold Blooded: " . $kodok->cold_blooded . "<br>";
echo "Jump: " . $kodok->jump() ; // "hop hop"



?>